#' helpers 
#'
#' @description A utils function
#'
#' @return The return value, if any, from executing the utility.
#'
#' @noRd

cbind.fill <- function(...){
    nm <- list(...) 
    nm <- lapply(nm, as.matrix)
    n <- max(sapply(nm, nrow)) 
    do.call(cbind, lapply(nm, function (x) 
        rbind(x, matrix(, n-nrow(x), ncol(x))))) 
}

# I wrote this one because shiny is really annoying with data types and it's hard
# to tell if they'll be NULL or NA or "NULL" or "NA" or an empty vector, so this
# covers all of them
valid <- function(x) {
  if ( is.null(x) ) return(FALSE)
  else if ( is.vector(x) & length(x) == 0 ) return(FALSE)
  else if ( is.na(x) ) return(FALSE)
  else if ( x== '' | x == 'NA' | x == 'NULL' ) return(FALSE)
  # if (isTruthy(x) & x != 'NA' & x != 'NULL' & !is.na(x) &  return(TRUE)
  else return(TRUE)
}